package main

import (
	"fmt"
	"os"

	"gitlab.com/gocodetest/chat-client/controller"
	"gitlab.com/gocodetest/chat-client/generator"
	"gitlab.com/gocodetest/chat-client/sender"
)

// RequestsCount defines the number of the requests the application has to send.
const RequestsCount = 1000000000

// ThreadsCount defines the number of gorutines the controller uses in order to send messages.
const ThreadsCount = 1000

func main() {
	g := generator.NewGenerator(RequestsCount)
	s := sender.NewHTTPSender(fmt.Sprintf("http://%s/message", os.Getenv("SERVER_ADDR")))
	c := controller.NewController(g, s, ThreadsCount)
	c.SendAll()
}
