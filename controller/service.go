// Package controller contains the application controller.
package controller

// Service describes the controller interface.
type Service interface {
	SendAll()
}
