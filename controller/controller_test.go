package controller

import (
	"errors"
	"sync"
	"testing"

	"gitlab.com/gocodetest/chat-client/generator"
	"gitlab.com/gocodetest/chat-client/model"
)

type testSender struct {
	count int64
	mx    sync.Mutex
}

func (t *testSender) Send(msg model.Message) error {
	var err error
	t.mx.Lock()
	t.count++
	if t.count == 1 {
		// one step tests the sender error
		err = errors.New("test err")
	}
	t.mx.Unlock()
	return err
}

func TestController_SendAll(t *testing.T) {
	messagesCount := int64(100000)
	threadsCount := 100
	g := generator.NewGenerator(messagesCount)
	s := &testSender{}
	ctrl := NewController(g, s, threadsCount)
	ctrl.SendAll()
	if messagesCount != s.count {
		t.Fatalf("expected %d messages; got %d", messagesCount, s.count)
	}
}
