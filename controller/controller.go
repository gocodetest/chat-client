package controller

import (
	"log"
	"sync"

	"gitlab.com/gocodetest/chat-client/generator"
	"gitlab.com/gocodetest/chat-client/model"
	"gitlab.com/gocodetest/chat-client/sender"
)

// NewController returns a new instance of the controller.
func NewController(g generator.Service, s sender.Service, threads int) Controller {
	return Controller{g: g, s: s, t: threads}
}

// Controller is in charge of performing application actions.
type Controller struct {
	g generator.Service
	s sender.Service
	t int
}

// SendAll sends all messages to the server.
func (c Controller) SendAll() {
	var wg sync.WaitGroup
	wg.Add(c.t)
	for i := 0; i < c.t; i++ {
		go c.sendAll(&wg)
	}
	wg.Wait()
}

func (c Controller) sendAll(wg *sync.WaitGroup) {
	var msg *model.Message
	for {
		msg = c.g.Message()
		if msg == nil {
			wg.Done()
			return
		}
		if err := c.s.Send(*msg); err != nil {
			log.Printf("error while sending message: %v\n", err)
		}
	}
}
