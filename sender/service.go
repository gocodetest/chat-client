// Package sender contains the client that should send messages to the server.
package sender

import "gitlab.com/gocodetest/chat-client/model"

// Service describes the interface of a messages sender.
type Service interface {
	Send(msg model.Message) error
}
