package sender

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/gocodetest/chat-client/model"
)

// NewHTTPSender returns a new instance of a HTTPSender.
func NewHTTPSender(url string) HTTPSender {
	return HTTPSender{
		url:    url,
		client: http.Client{},
	}
}

// HTTPSender is a client that sends the messages over HTTP.
type HTTPSender struct {
	url    string
	client http.Client
}

// Send sends message over HTTP.
func (s HTTPSender) Send(msg model.Message) error {
	data, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("error while marshaling the message: %w", err)
	}
	r, err := http.NewRequest(http.MethodPost, s.url, bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("error while creating new http request: %w", err)
	}
	r.Header.Add("Content-Type", "application/json")
	resp, err := s.client.Do(r)
	if err != nil {
		return fmt.Errorf("error while performing http request: %w", err)
	}
	// nolint:gosec // it doesn't make sense to handle this error
	resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response status %d", resp.StatusCode)
	}
	return nil
}
