// Package generator contains the messages generator.
package generator

import "gitlab.com/gocodetest/chat-client/model"

// Service describes the interface of a messages generator.
type Service interface {
	Message() *model.Message
}
