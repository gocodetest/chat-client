package generator

import (
	"sync"
	"testing"
)

func TestGenerator_Message(t *testing.T) {
	type res struct {
		mx    sync.Mutex
		count int64
	}

	target := int64(100000)
	var got res
	threads := 100
	gen := NewGenerator(target)
	var wg sync.WaitGroup
	wg.Add(threads)
	for i := 0; i < threads; i++ {
		go func() {
			for {
				if gen.Message() == nil {
					wg.Done()
					return
				}
				got.mx.Lock()
				got.count++
				got.mx.Unlock()
			}
		}()
	}
	wg.Wait()

	if target != got.count {
		t.Fatalf("expected %d messages; got %d", target, got.count)
	}
}
